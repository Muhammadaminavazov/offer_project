<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 0; $i < 10; $i++) {
            DB::table('products')->insert([
                'model_name' => 'Apple watch',
                'description' => Str::random(50),
                'product_img' => Str::random(8) . 'jpg',
                'category_id' => '2',
                'price' => '200'

            ]);
        }
    }
}