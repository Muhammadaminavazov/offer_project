<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TableController;

use Illuminate\Support\Facades\Auth;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [TableController::class, 'userWelcome']);

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

// Table
Route::get('/table', [TableController::class, 'index']); //table index
Route::post('/tableUpdate', [TableController::class, 'store']); //table update information
Route::get('/ajax', [TableController::class, 'ajax']);//ajax all post
Route::get('/deleteTable/{id}', [TableController::class, 'delete']);//delete post
Route::post('/tableEdit/{id}', [TableController::class, 'edit']); //table edit information
Route::get('/allDelete/{id}', [TableController::class, 'allDelete']);//delete all checked
Route::get('/oneProduct/{id}', [TableController::class,'oneProduct']); //return one product by id

Route::get('/ajaxpaginate', [TableController::class, 'ajaxpaginate']);//ajax all post

// function checkboxDelete() {
//     this.deleteCheck = document.querySelectorAll('.deleteCheck');
//     deleteCheck.forEach(function (checkbox) {
//         checkbox.addEventListener('change', function () {
//             if (checkbox.checked == true) {
//                 let user = data
//                 deletes.push(checkbox.value)
//                 let delete_filter = user.filter(a => a.id == checkbox.value)
//                 if (delete_users.filter(v => v.id == checkbox.value).length < 1) {
//                     delete_users.push(delete_filter[0])
//                 }
//                 allDeleteBtn.style.display = 'block';
//             } else {
//                 deletes = deletes.filter(e => {
//                     if (e != checkbox.value)
//                         return e
//                 })
//                 // check remove
//                 delete_users = delete_users.filter(remove => {
//                     if (remove.id != checkbox.value) {
//                         return remove
//                     }
//                 })
//                 if (deletes.length == 0) {
//                     allDeleteBtn.style.display = 'none';
//                 }
//             }

//         })
//     })
// }
