let data;
const allproductView = document.getElementById('all_products');
const categories = document.getElementById('categories');

var currentPage = 1;
const perPage = 1;
var totalItems = 0;
var totalPages = 0;


function getData(page) {
  // Create new XMLHttpRequest object
  const xhr = new XMLHttpRequest();

  // Set up request URL with page and perPage parameters
  const url = `http://127.0.0.1:8000/ajaxpaginate?page=${page}&perPage=${perPage}`;

  // Set up request method and URL
  xhr.open('GET', url);

  // Set up onload function to handle response
  xhr.onload = function() {
    if (xhr.status === 200) {
      const response = JSON.parse(xhr.response);
       data = response.data.data;
      totalItems = response.data.total;

      totalPages = Math.ceil(totalItems / perPage);
      currentPage = page;

      // Call displayData function to display data on page
       productShow(data, 'all');
      showCategories(response.category);


      // Call createPagination function to create pagination links
      createPagination();
    } else {
      console.error(xhr.statusText);
    }
  };

  // Send request
  xhr.send();
}

function createPagination() {
  var pagination = document.getElementById('pagination');
  pagination.innerHTML='';
// console.log(pagination.innerHTML);
  for (let i = 1; i <= totalPages; i++) {
    const link = document.createElement('a');
    link.href = '#';
    link.textContent = i;
    link.onclick = function() {
    getData(i);
    };
    if (i === currentPage) {
      link.className = 'active';
    }
    pagination.appendChild(link);
  }
}
// async function getData() {
//     try {
//         const response = await fetch(`http://127.0.0.1:8000/ajaxpaginate?page=${page}&perPage=${perPage}`);
//         const json = await response.json();
//         data = json.data;
//         showCategories(json.category);
//         await productShow(data, 'all');
//     } catch (error) {
//         console.error(error);
//     }
// }


async function productShow(info, category) {
    const itemsHTML = info
        .filter((element) => category === 'all' || element.category_id === category)
        .map(
            (element) => `
        <li class="product">
          <div class="image-wrapper"><img src="storage/images/${element.product_img}" alt="camera"></div>
          <div class="product-body">
            <h5>${element.model_name}</h5>
            <div class="product-price">$${element.price}</div>
            <a href="/product/${element.id}/detail" class="product-view-btn">View more</a>
          </div>
        </li>
      `
        )
        .join('');
    allproductView.innerHTML = itemsHTML;
}

// const categories = document.getElementById('categories'); // assuming the element has an ID of 'categories'
function showCategories(info) {
    const categoryHTML = `<button class="category-btn" data-category="all">All</button>` + info
        .map((element) => `<button class="category-btn" data-category="${element.id}">${element.name}</button>`)
        .join('');
    categories.innerHTML = categoryHTML;
    const categoryBtns = categories.querySelectorAll('.category-btn');
    categoryBtns.forEach((btn) => {
        btn.addEventListener('click', () => {
            categoryBtns.forEach((btn) => btn.classList.remove('active'));
            btn.classList.add('active');
            const category = btn.dataset.category;
            // assuming productShow is defined elsewhere
            productShow(data, category);
        });
    });
}


window.addEventListener('DOMContentLoaded', getData);



var search = document.getElementById('search')
search.addEventListener('keyup', function () {
    const filter = search.value.toUpperCase();
    const d = data.filter(function (val) {
        if (val.model_name.toUpperCase().indexOf(filter) > -1)
            return val;
    });
    productShow(d, 'all');
});
