// Variables
var data;
var category;
var limit = 1;
var table = document.getElementById('forTable');
var search = document.getElementById('search');
// Form elements
var form = document.getElementById('form');
var editForm = document.getElementById('editFrom');
var editName = document.getElementById('editName');
var editSurname = document.getElementById('editSurname');
var deleteText = document.getElementById('deleteText');
// Buttons
var addSubmit = document.getElementById('addBtn');
var editBtn = document.getElementById('editBtn');
var deleteBtn = document.getElementById('deleteBtn');
var allDeleteBtn = document.getElementById('allDeleteBtn');
// Input form elements
var productName = document.getElementById('modelName');
var description = document.getElementById('description');
var price = document.getElementById('price');
var category = document.getElementById('category');
var productImg = document.getElementById('product_img');
// Delete check variables
var deletes = [];
var delete_users = [];
// Function to display table data
function chiz(info, category, a) {
    var limit = 1;
    table.innerHTML = '';
    info.forEach(element => {
        var row = document.createElement('tr');
        row.className = 'tr-table';
        row.id = 'sortable';
        var checkCell = document.createElement('td');
        checkCell.className = 'check';
        var checkBox = document.createElement('input');
        checkBox.type = 'checkbox';
        checkBox.className = 'deleteCheck';
        checkBox.value = element.id;
        checkBox.name = 'checkName';
        checkCell.appendChild(checkBox);
        var limitCell = document.createElement('td');
        limitCell.textContent = limit++;
        var modelNameCell = document.createElement('td');
        modelNameCell.textContent = element.model_name;
        var descriptionCell = document.createElement('td');
        descriptionCell.textContent = element.description.substring(0, 30);
        var categoryCell = document.createElement('td');
        categoryCell.textContent = category.filter(a => a.id == element.category_id)[0].name;
        var priceCell = document.createElement('td');
        priceCell.textContent = element.price;
        var actionCell = document.createElement('td');
        actionCell.className = 'action';
        var deleteButton = document.createElement('button');
        deleteButton.className = 'act-btn';
        deleteButton.setAttribute('data-bs-toggle', 'modal');
        deleteButton.setAttribute('data-bs-target', '#exampleModalDelete');
        deleteButton.onclick = function () {
            deleteForm(element.id);
        };
        var deleteIcon = document.createElement('i');
        deleteIcon.className = 'bx bx-trash';
        deleteButton.appendChild(deleteIcon);
        var editButton = document.createElement('button');
        editButton.className = 'act-btn edit-product';
        editButton.setAttribute('data-bs-toggle', 'modal');
        editButton.setAttribute('data-bs-target', '#exampleModalEdit');
        editButton.onclick = function () {
            editProduct(element.id);
        };
        var editIcon = document.createElement('i');
        editIcon.className = 'bx bx-edit';
        editButton.appendChild(editIcon);
        actionCell.appendChild(deleteButton);
        actionCell.appendChild(editButton);
        row.appendChild(checkCell);
        row.appendChild(limitCell);
        row.appendChild(modelNameCell);
        row.appendChild(descriptionCell);
        row.appendChild(categoryCell);
        row.appendChild(priceCell);
        row.appendChild(actionCell);
        table.appendChild(row);
    });
    checkboxDelete();
}
// Function to get data
function getData() {
    const http = new XMLHttpRequest();
    http.onload = function () {
        data = JSON.parse(this.response).data;
        category = JSON.parse(this.response).category;
        chiz(data, category);
    };
    http.open('GET', 'http://127.0.0.1:8000/ajax');
    http.send();
}
// Event listener for DOMContentLoaded
window.addEventListener("DOMContentLoaded", function () {
    getData();
});
// Event listener for search
search.addEventListener('keyup', function () {
    const filter = search.value.trim().toUpperCase();
    if (!filter) {
        // handle empty input
        return;
    }
    const d = data.filter(function (val) {
        return val.model_name && val.description &&
            (val.model_name.toUpperCase().includes(filter) ||
                val.description.toUpperCase().includes(filter));
    });
    chiz(d, category);
});
// Event listener for addSubmit button
addSubmit.addEventListener('click', function () {
    const xtp = new XMLHttpRequest();
    const formData = new FormData(form);
    xtp.open('POST', 'http://127.0.0.1:8000/tableUpdate');
    xtp.send(formData);
    xtp.onload = function () {
        if (productName.value.length > 0 && description.value.length > 0 && price.value.length > 0 && productImg.files.length > 0) {
            productName.placeholder = 'Success';
            price.placeholder = 'Success';
            productName.value = '';
            description.value = '';
            price.value = '';
            category.value = '';
            productImg.value = '';
        } else {
            productName.placeholder = 'Malumot kiriting';
            price.placeholder = 'Malumot kiriting';
            description.placeholder = 'Malumot kiriting';
        }
        getData();
    }
});
var keepId;

function editProduct(productId) {
    if (!productId || typeof productId !== 'number') {
        console.error('Invalid productId');
        return;
    }
    fetch('/oneProduct/' + productId)
        .then(response => {
            if (!response.ok) {
                throw new Error('Network response was not ok');
            }
            return response.json();
        })
        .then(data => {
            var productData = data.data;
            var category = data.category;
            var model_nameEdit = document.getElementById('model_nameEdit');
            var priceEdit = document.getElementById('priceEdit');
            var descriptionEdit = document.getElementById('descriptionEdit');
            var categoryEdit = document.getElementById('categoryEdit');
            var oldProductImg = document.getElementById('old_product_img');
            var productImgEdit = document.getElementById('product_imgEdit');
            var product_img_preview = document.getElementById('product_img_preview');
            if (productData) {
                model_nameEdit.value = productData.model_name || '';
                priceEdit.value = productData.price || '';
                descriptionEdit.value = productData.description || '';
                categoryEdit.innerHTML = '';
                category.forEach(cat => {
                    var option = document.createElement('option');
                    option.value = cat.id;
                    option.text = cat.name;
                    if (cat.id === productData.category_id) {
                        option.selected = true;
                    }
                    categoryEdit.add(option);
                });
                if (productData.product_img) {
                    oldProductImg.setAttribute('src', 'storage/images/' + productData.product_img);
                    oldProductImg.style.display = 'block';
                }
                productImgEdit.addEventListener('change', function () {
                    var file = this.files[0];
                    var reader = new FileReader();
                    reader.onloadend = function () {
                        product_img_preview.setAttribute('src', 'storage/images/' + reader.result);
                    }
                    reader.readAsDataURL(file);
                });
            }
            keepId = productId
        })
        .catch(error => {
            console.error('Error fetching product data:', error);
        });


}


// Function to save edited product data
function saveProduct(productId) {
    var formEditData = document.getElementById('edit-product-form');
    var formData = new FormData(formEditData);
    // Validate form data
    if (formData.get('productName') === '' || formData.get('productPrice') === '') {
        alert('Please fill in all required fields.');
        return;
    }
    var xhr = new XMLHttpRequest();
    xhr.open('POST', '/tableEdit/' + productId, true);
    xhr.onload = function () {
        if (this.status == 200) {
            getData();
        }
    }
    xhr.send(formData);
}

// Event listener for save product button
document.getElementById('save-product').addEventListener('click', function () {
    // var productId = document.getElementById('edit-product-form').dataset.id;
    var id = keepId
    saveProduct(id);
});

function checkboxDelete() {
    this.deleteCheck = document.querySelectorAll('.deleteCheck');
    for (let i = 0; i < this.deleteCheck.length; i++) {
        const checkbox = this.deleteCheck[i];
        checkbox.addEventListener('change', function () {
            if (checkbox.checked) {
                let user = data;
                deletes.push(checkbox.value);
                let delete_filter = user.filter(a => a.id == checkbox.value);
                if (!delete_users.some(v => v.id == checkbox.value)) {
                    delete_users.push(delete_filter[0]);
                }
                allDeleteBtn.style.display = 'block';
            } else {
                deletes = deletes.filter(e => e != checkbox.value);
                delete_users = delete_users.filter(remove => remove.id != checkbox.value);
                if (deletes.length == 0) {
                    allDeleteBtn.style.display = 'none';
                }
            }
        });
    }
}
// Function to show delete form
function deleteForm(val) {
    var del_filter = data.filter(a => a.id == parseInt(val));
    var delete_show = del_filter.map(a => `
    <div style="font-weight: 700">
    <p class="deleteFormText">Model Name: ${escapeHtml(a.model_name)}</p>
    <p class="deleteFormText">Price: ${escapeHtml(a.price)}</p>
    <p class="deleteFormText">Description: ${escapeHtml(a.description.substring(0, 35))} </p>
    <img src="storage/images/${escapeHtml(a.product_img)}" alt="" class="deleteFormImg" >
 </div>
    `).join('');
    deleteText.innerHTML = delete_show;
    one_id = parseInt(val);
    deleteBtn.innerText = 'Delete';
}

function escapeHtml(text) {
    var map = {
        '&': '&amp;',
        '<': '&lt;',
        '>': '&gt;',
        '"': '&quot;',
        "'": '&#039;'
    };
    return text.replace(/[&<>"']/g, function (m) {
        return map[m];
    });
}
// Event listener for all delete button
var btnal;
allDeleteBtn.addEventListener('click', function () {
    allDeleteFunc()
    btnal = deleteBtn.innerText = 'Deletes';
})
// Function to show all delete form
function allDeleteFunc() {
    var deleteData = delete_users.map(a => `
    <div style="font-weight: 700; font-size:14px">
    <p class="deleteFormText">Model Name: ${escapeHtml(a.model_name)}</p>
    <p class="deleteFormText">Price: ${escapeHtml(a.price)}</p>
    <p class="deleteFormText">Description: ${escapeHtml(a.description.substring(0, 60))}</p>
    <img src="storage/images/${escapeHtml(a.product_img)}" alt="" class="allDeleteFormImg">
 </div>
    `)
    deleteData = deleteData.join('')
    deleteText.innerHTML = deleteData
}
// Function to escape HTML special characters
function escapeHtml(text) {
    var map = {
        '&': '&amp;',
        '<': '&lt;',
        '>': '&gt;',
        '"': '&quot;',
        "'": '&#039;'
    };
    return text.replace(/[&<>"']/g, function (m) {
        return map[m];
    });
}
// Event listener for delete button
deleteBtn.addEventListener('click', function () {
    let id = parseInt(deletes);
    deletes = [];
    delete_users = [];
    const fetchOptions = {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    };
    if (btnal == 'Deletes') {
        fetch('https://127.0.0.1:8000/allDelete/' + id, fetchOptions)
            .then(response => {
                if (!response.ok) {
                    throw new Error('Network response was not ok');
                }
                return response.json();
            })
            .then(data => {
                getData();
                allDeleteBtn.style.display = 'none';
            })
            .catch(error => {
                console.error('There was a problem with the fetch operation:', error);
            });
    } else {
        fetch('https://127.0.0.1:8000/deleteTable/' + one_id, fetchOptions)
            .then(response => {
                if (!response.ok) {
                    throw new Error('Network response was not ok');
                }
                return response.json();
            })
            .then(data => {
                getData();
                allDeleteBtn.style.display = 'none';
            })
            .catch(error => {
                console.error('There was a problem with the fetch operation:', error);
            });
    }
})
// Sortable
const sortable = document.getElementById('forTable');
new Sortable(sortable, {
    Animation: 350,
    ghostClass: 'sortable'
});
