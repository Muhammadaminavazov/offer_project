<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Cabin Fonts Family -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link
        href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap"
        rel="stylesheet">
    <!-- CSS -->
    <link rel="stylesheet" href="css/product.css">
    <title>product description</title>
</head>

<body>
    <div class="container">
        <a class="home-btn" href="../index.html"><img src="../images/arrow-left.png" alt=""> Back</a>
        <div class="product-credentials">
            <img src="./images/apple-watch.png" alt="product">
            <div class="product-values">
                <h3 class="product-name">Apple Watch</h3>
                <!-- <h5 class="product-series">Series 5 SE</h5> -->
                <!-- <div class="ratings-wrapper">
                    <img src="../images/Rating.png" alt="rating">
                    <span class="rating">4.5 / 5</span>
                </div> -->
                <p class="product-price">$529.99</p>
                <p class="product-mini-description">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quis
                    pellentesque tellus imperdiet mattis. Proin in quis ipsum non amet imperdiet. Dignissim nisi leo a
                    at. Sit nec lacus, nunc volutpat, tincidunt lorem mi duis. Vitae elementum libero.</p>
                <button class="contact-btn">Bog'lanish</button>
            </div>
        </div>
        <div class="product-description">
            <!-- <h3>Description</h3> -->
            <!-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Dignissim odio faucibus nec malesuada purus
                volutpat vel sed viverra. Id sagittis, phasellus dui in arcu. Nec arcu, sit nunc, nibh purus
                pellentesque sagittis. Felis rhoncus facilisis massa eget purus in purus. Etiam at cras nulla nunc.
                Malesuada in pretium diam scelerisque sit mattis in egestas neque. Eu porta tempor sodales nisl integer
                turpis porttitor sed sed. Ut senectus odio dictum enim velit tempor diam quisque suspendisse.</p>
            <p>Orci vel ridiculus diam viverra. Libero malesuada orci, quis placerat suscipit augue imperdiet. Et
                praesent augue dictum mauris eget lacus malesuada. Aenean nisi, sodales natoque massa magna dignissim
                mi. Mattis tellus, justo, lorem sed tempor diam sit viverra enim. Id id placerat eu etiam nulla laoreet.
            </p>
            <p>Dignissim leo fames turpis quis suspendisse vulputate laoreet vulputate ac. Aliquam justo lectus eu dui
                porttitor. Cras a aliquam phasellus sollicitudin ornare. Tristique volutpat facilisis in ut proin. Est
                vitae facilisi sollicitudin id lorem mattis nibh ipsum, nec. Consectetur consectetur morbi morbi aliquet
                mi risus, velit, sit at. Integer morbi viverra hendrerit risus.</p>
            <p>Odio phasellus nibh senectus nec id enim quam sed. At potenti sollicitudin sollicitudin lobortis morbi.
                Nunc molestie et adipiscing aliquam. Sit vel mi dolor suscipit. In eget ut ac at facilisi leo viverra.
                Arcu ac ut fermentum, viverra et, vitae etiam cras. Eu purus non ut turpis fusce. Mi vitae nibh mi ut
                feugiat varius risus eros.</p> -->
        </div>
    </div>
</body>

</html>
