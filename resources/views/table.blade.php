@extends('layouts.app')

@section('content')
    <h3 class="text-center">Products tabel</h3>
    <hr>
    <div class="nav-table">
        <form id="form" method="POST" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
            <input class="input" type="text" placeholder="Model name" name="modelName" id="modelName" required>
            <input class="input" type="text" placeholder="Price" name="price" id="price" required>
            <select id="category" class="input" name="category" required>
                <option selected>Category</option>
                @foreach ($category as $c)
                    <option value="{{ $c->id }}"> {{ $c->name }}</option>
                @endforeach

            </select>
            <div class="form-floating desciptionInput">
                <textarea class="form-control int" placeholder="Leave a desciption here" id="description" name="description"></textarea>
                <label for="floatingTextarea">Desciption</label>
            </div>
            {{-- <input class="input" type="text" placeholder="Your surname" name="lastName" id="lastName" required> --}}
            <input class="input-file input" id="product_img" type="file" name="product_img" required>
            <label tabindex="0" for="product_img" class="input-file-trigger">Select a file...</label>
            <button type="button" class="btn btn-success" id="addBtn">Submit</button>
        </form>
    </div>
    <div class="nav-body bg-slate">
        <table class="table">
            <thead>
                <div class="table-serach">
                    <h3>There are  yor products </h3>
                    <input class="input" type="text" placeholder="Enter the search ..." id="search">
                </div>
                <tr class="table-th">
                    <th scope="col" class="width-check">
                        {{-- <input type="checkbox" id="allDeleteCheck" onClick="toggle(this)"> --}}
                    </th>
                    <th scope="col" class="width">No</th>
                    <th>Model Name</th>
                    <th>Description</th>
                    <th>Category</th>
                    <th>Pirce</th>
                    <th scope="col" class="width">Action</th>
                </tr>
            </thead>
            <tbody id="forTable" class="tbody">
                {{-- Table ajax return --}}
            </tbody>
        </table>
        <div class="table-footer">
            <nav aria-label="Page navigation example">
                <ul class="pagination">
                    <li class="page-item"><a class="page-link" href="#">Prev</a></li>
                    <li class="page-item"><a class="page-link" href="#">1</a></li>
                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                    <li class="page-item"><a class="page-link" href="#">Next</a></li>
                </ul>
            </nav>
            <button type="button" class="btn btn-danger" id="allDeleteBtn" data-bs-toggle="modal"
                data-bs-target="#exampleModalDelete">Delete All</button>
        </div>
    </div>


@endsection
