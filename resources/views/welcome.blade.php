<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Poppins Fonts Family -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link
        href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap"
        rel="stylesheet">
    <!-- Almarai Fonts Family -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link
        href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap"
        rel="stylesheet">
    <!-- CSS -->
    <link rel="stylesheet" href="css/styles.css">
    <title>Equip</title>
</head>

<body>
    <!-- Header section -->
    <header>
        <div class="container header-container">
            <a href="#" class="site-logo">Equip</a>
        </div>
    </header>
    <main>
        <!-- Hero section -->
        <section>
            <div class="container hero-container">
                <h1>Create, Manage & Boost Your Business online store namuna</h1>
                <button class="start-btn">Boshlash</button>
            </div>
        </section>

        <!-- search and categories section -->
        <section>
            <div class="container">
                <!-- search -->
                <div class="search">
                    <input type="text" placeholder="Search" class="search-input" id="search">
                    <div class="categories-wrapper">
                        <!-- categories -->
                        <ul class="categories" id="categories">

                        </ul>
                        <span class="categories-more">more</span>
                    </div>
                </div>

                <!-- products -->
                <h3 class="category-name">All</h3>
                <ul class="all-products" id="all_products">
                    <!-- fake products -->


                </ul>

                <div class="pagination" id="pagination">
                    {{-- <img src="./images/arrow-left.png" alt="pagination-arrow"> --}}
                    {{-- <ul class="pages" id="pagination">
                        <li class="active-page">1</li>
                        <li>2</li>
                        <li>3</li>
                        <li>4</li>
                        <li>5</li>
                        <li>6</li>
                        <li>7</li>
                    </ul> --}}
                    {{-- <img src="./images/arrow-rigth.png" alt="pagination-arrow"> --}}
                </div>
            </div>
        </section>
    </main>

    <footer>
        <div class="container footer-container">
            <a href="#" class="site-logo">Equip</a>
            <h5 class="footer-desc">Open surce is source code that is made freely avilable for possible modification
                and
                reducation. Product include premadoas to us </h5>
        </div>
        <p class="security">All Rights Reserved</p>
    </footer>

    <script src="js/user.js"></script>
</body>

</html>
