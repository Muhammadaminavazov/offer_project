<?php
namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Products;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Response;

class TableController extends Controller
{
    /**
     * Return products and categories as JSON.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax()
    {
        $table = Products::orderBy('id', 'desc')->get();
        $category = Category::all();
        return Response::json([
            'data' => $table,
            'category' => $category
        ], 200);
    }

    public function ajaxpaginate(Request $request)
    {
        $page = $request->input('page', 1);
        $perPage = $request->input('perPage', 10);
        $table = Products::orderBy('id', 'desc')->paginate($perPage);
        $category = Category::all();
        return Response::json([
            'data' => $table,
            'category' => $category
        ], 200);
    }
    /**
     * Return the table view with categories.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function index()
    {
        $category = Category::all();
        return view('table', ['category' => $category]);
    }
    /**
     * Store a new product.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'modelName' => 'required|string|max:255',
            'description' => 'required|string',
            'category' => 'required|exists:categories,id',
            'price' => 'required|numeric|min:0',
            'product_img' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        $table = new Products;
        $table->model_name = $validated['modelName'];
        $table->description = $validated['description'];
        $table->category_id = $validated['category'];
        $table->price = $validated['price'];
        $input = $request->file('product_img');
        $filename = time() . '.' . $input->getClientOriginalExtension();
        $input->storeAs('/images/', $filename, 'public');
        // Storage::disk('public')->putFileAs('images', $input, $filename);
        $table->product_img = $filename;
        $table->save();
        return $table;
    }
    /**
     * Delete a product and its image.
     *
     * @param int $id
     * @return int
     */
    public function delete($id)
    {
        $product = Products::findOrFail($id);
        Storage::disk('public')->delete('/images/' . $product->product_img);
        $product->delete();
        return 1;
    }
    /**
     * Update a product.
     *
     * @param int $id
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Database\Eloquent\Model
     */

    public function edit($id, Request $request)
    {
        $validated = $request->validate([
            'model_name' => 'required|string|max:255',
            'description' => 'required|string',
            'category_id' => 'required|exists:categories,id',
            'price' => 'required|numeric|min:0',
            'product_img' => 'nullable|image|max:2048',
        ]);

        $table = Products::findOrFail($id);
        $table->model_name = $validated['model_name'];
        $table->description = $validated['description'];
        $table->category_id = $validated['category_id'];
        $table->price = $validated['price'];

        // Delete old product image if new one is uploaded
        if ($request->hasFile('product_img')) {
            Storage::disk('public')->delete('/images/' . $table->product_img);
        }

        // Upload new product image if one is uploaded
        if ($request->hasFile('product_img')) {
            $input = $request->file('product_img');
            $filename = time() . '.' . $input->getClientOriginalExtension();
            $input->storeAs('/images/', $filename, 'public');
            $table->product_img = $filename;
        }

        $table->save();

        return $table;
    }
    public function oneProduct($id)
    {
        $product = Products::findOrFail($id);
        $category = Category::all();

        return Response::json([
            'data' => $product,
            'category' => $category
        ], 200);

    }
    /**
     * Delete multiple products.
     *
     * @param string $id
     * @return int
     */
    public function allDelete($id)
    {
        // Validate the input
        $all_id = array_filter(explode(',', $id), function ($item) {
            return is_numeric($item);
        });
        // Delete the images first
        $products = Products::whereIn('id', $all_id)->get();
        foreach ($products as $product) {
            Storage::disk('public')->delete('/images/' . $product->product_img);
        }
        // Delete the products
        Products::whereIn('id', $all_id)->delete();
        return count($all_id);
    }
    public function userWelcome()
    {
        $products = Products::all();
        $category = Category::all();
        return view('welcome');
    }

}
