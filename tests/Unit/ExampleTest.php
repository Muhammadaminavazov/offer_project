<?php

namespace Tests\Unit;
 use App\Models\Category;
use App\Models\Products;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;
 class TableControllerTest extends TestCase
{
    use RefreshDatabase;
     public function test_ajax()
    {
        $category = Category::factory()->create();
        $product = Products::factory()->create(['category_id' => $category->id]);
         $response = $this->json('GET', '/ajax');
         $response->assertStatus(200)
            ->assertJson([
                'data' => [
                    [
                        'id' => $product->id,
                        'model_name' => $product->model_name,
                        'description' => $product->description,
                        'category_id' => $product->category_id,
                        'price' => $product->price,
                        'product_img' => $product->product_img,
                    ]
                ],
                'category' => [
                    [
                        'id' => $category->id,
                        'name' => $category->name,
                    ]
                ],
            ]);
    }
     public function test_index()
    {
        $response = $this->get('/table');
         $response->assertStatus(200)
            ->assertViewIs('table');
    }
     public function test_store()
    {
        Storage::fake('public');
        $category = Category::factory()->create();
         $response = $this->post('/store', [
            'modelName' => 'Test Model',
            'description' => 'Test Description',
            'category' => $category->id,
            'price' => 100,
            'product_img' => UploadedFile::fake()->image('product.jpg'),
        ]);
         $response->assertStatus(201);
         $this->assertDatabaseHas('products', [
            'model_name' => 'Test Model',
            'description' => 'Test Description',
            'category_id' => $category->id,
            'price' => 100,
        ]);
         $product = Products::first();
        Storage::disk('public')->assertExists('/images/' . $product->product_img);
    }
     public function test_delete()
    {
        Storage::fake('public');
        $product = Products::factory()->create();
         Storage::disk('public')->put('/images/' . $product->product_img, '');
         $response = $this->delete('/delete/' . $product->id);
         $response->assertStatus(200);
         $this->assertDatabaseMissing('products', [
            'id' => $product->id,
        ]);
         Storage::disk('public')->assertMissing('/images/' . $product->product_img);
    }
     public function test_edit()
    {
        $product = Products::factory()->create();
        $category = Category::factory()->create();
         $response = $this->put('/edit/' . $product->id, [
            'modelName' => 'Updated Model',
            'description' => 'Updated Description',
            'category' => $category->id,
            'price' => 200,
        ]);
         $response->assertStatus(200);
         $this->assertDatabaseHas('products', [
            'id' => $product->id,
            'model_name' => 'Updated Model',
            'description' => 'Updated Description',
            'category_id' => $category->id,
            'price' => 200,
        ]);
    }
     public function test_allDelete()
    {
        $products = Products::factory()->count(3)->create();
        $ids = $products->pluck('id')->implode(',');
         $response = $this->delete('/allDelete/' . $ids);
         $response->assertStatus(200);
         foreach ($products as $product) {
            $this->assertDatabaseMissing('products', [
                'id' => $product->id,
            ]);
        }
    }
}